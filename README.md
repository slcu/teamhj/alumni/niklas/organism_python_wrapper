# A python wrapper for Organism
## Organism
[Organism](https://gitlab.com/slcu/teamHJ/Organism) is a C++ program for simulating reactions and growth in biological 
tissue. It can be currently be obtained over Git after communcating with 
Henrik Jönsson at The Sainsbury Laboratory, University of Cambridge.

## This library
This is a library of Python functions to facilitate scripting with Organism.

Organism relies on a bunch of provided configuration files. 
This library allows you to programmatically create and use temporary such files. 
It also provides functions to run Organism and collect its output into python objects which are easy to work with. 

## Installation and module import
To install, simply clone the library to some suitable location. 
Navigate to your choosen location.
If you have set up SSH for gitlab enter:
``` bash
git clone git@gitlab.com:slcu/teamHJ/niklas/organism_python_wrapper.git organism
```

Otherwise you can instead enter:
``` bash
git clone https://gitlab.com/slcu/teamHJ/niklas/organism_python_wrapper.git organism
```

In the same folder that you preformed the above operation, create an empty __init__.py file:
``` bash
echo '' >> __init__.py
```

Next, update your PYTHONPATH variable with the location of the library (the folder with the __init__.py): 
``` bash
export PYTHONPATH=$PYTHONPATH:/location/to/your/library/
```

To avoid having to repeat this when you restart your terminal, you could append that line to the configuration file of your shell (usually ~/.bash_profile). 

Now you can import the library to a python session with something like:
``` python
import organism.organism as o
```
