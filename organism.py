import numpy as np
import tempfile
import subprocess as sub
import doctest
import copy


def clean_file_parsing(file_object):
    """ A generator which removes blank lines and comments from file or string 
    input.
    
    :file_object: an object from open(file)

    """
    for line in file_object:
        line = line.partition('#')[0] # Remove comments
        line = line.rstrip() # Remove trailing whitespaces
        if line: # If there is anything left
            yield line


def read_init(file_path):
    """ Read an init file and return an array of init values.

    This does not have perfect fidelity to the Organism parser.
    This function depends on there being linebreaks between cells.
    However, the functions checks for a number of potential errors.

    :file_path: sting specifying where to read the init file from.
    :returns: init -- a 2d np.array of init values. 
        init.shape[0] is cells
        init.shape[1] is the concentrations within cells.
    """
    init = []
    line_number=0
    header_line = []
    with open(file_path, 'r') as init_file:
        for line in clean_file_parsing(init_file): 
            # The first line specifies the shape of the final array
            if line_number == 0:
                header_line=[int(f) for f in line.split()]
                assert len(header_line) == 2
            # The other lines specify the species for each cell
            else:
                init.append([f for f in line.split()])
                assert len(init[-1])==header_line[1], \
                        "The specified and the actual number of species"\
                        + "does not match"
            line_number += 1 
        #init = np.array(init, ndmin=2)
        assert len(init) == header_line[0], \
                "Mismatch between specified and actual number of cells in init."

    return init


def read_model(file_path):
    """ Read an organism model file into a string.


    :file_path: string specifying where to read the solver file from.
    :returns: model -- a string.
    """
    with open(file_path, 'r') as model_file:
        model = model_file.read()
    return model


def read_solver(file_path):
    """ Read an organism solver file and return a list of strings 

    :file_path: string specifying where to read the solver file from.
    :returns: solver -- a list of strings from the solver file.
    """
    solver=[]
    with open(file_path, 'r') as solver_file:
        for line in clean_file_parsing(solver_file):
            solver.append([entry for entry in line.split()])

    solver = [ entry for sublist in solver for entry in sublist] # Flatten
    return solver


def write_init(file_obj, init):
    """TODO: Docstring for write_init.

    :file_obj: TODO
    :init: TODO
    :returns: TODO

    """
    file_obj.write('{} {} \n'.format(len(init), len(init[0])))
    for init_line in init:
        file_obj.write(' {} '.format(' '.join([str(i) for i in init_line])))
        file_obj.write("\n")
    file_obj.flush()


def write_solver(file_obj, solver):
    file_obj.write(" ".join([str(i) for i in solver]))
    file_obj.flush()

def write_estimator(file_obj, estimator):
    file_obj.write(" ".join([str(i) for i in estimator]))
    file_obj.flush()

def get_cost(estimator_binary, model, solver, estimator, param=None):
    """Use the organism estimator to get a cost value for a parameter set.

    :estimator_binary: path to estimator. organism/bin/estimator
    :model: (string) which model file to use.
    :solver: list specifying the solver to use. The solver must be made for 
             cost calculations. 
    :estimator: (string) which estimator file to use.
    :param: 1D list or array of parameter values.
    :returns: cost (float), std_out, std_err

    """
    
    with tempfile.NamedTemporaryFile(mode='w') as solver_tempfile, \
         tempfile.NamedTemporaryFile(mode='w') as model_tempfile, \
         tempfile.NamedTemporaryFile(mode='w') as estimator_tempfile, \
         tempfile.NamedTemporaryFile(mode='w') as param_tempfile: 
        write_solver(solver_tempfile, solver)
        model_tempfile.write(model)
        model_tempfile.flush()



        tempfiles = [tempfile.NamedTemporaryFile(mode="w") for i in 
                    range(estimator.nr_files_needed())]

        estimator.write_to_file(estimator_tempfile, tempfiles)
        #write_estimator(estimator_tempfile, estimator)

        command = [
                estimator_binary, 
                model_tempfile.name, 
                solver_tempfile.name,
                estimator_tempfile.name
                ]
        if param != None:
            param_tempfile.write(' '.join(str(i) for i in param))
            param_tempfile.flush()
            command.append('-parameter_input')
            command.append(param_tempfile.name)
        else:
            print("WARNING, the estimator used the parameters which were")
            print("defined straight in the model file.")

        simulation = sub.Popen(command, stdout=sub.PIPE, stderr=sub.PIPE)
        sim_std_out, sim_std_err = simulation.communicate()

        sim_std_out = sim_std_out.decode() # python 3 compatibility
        sim_std_err = sim_std_err.decode() # python 3 compatibility

        for f in tempfiles:
            f.close()

        if simulation.returncode != 0: # Check if the simulator ended with an error.
            err_msg = "{} \nExit code: {}".format(sim_std_err.strip(), 
                                                simulation.returncode)
            raise Exception(err_msg)

        cost = sim_std_err.split('\n')[-2] 
        cost = cost.split(' = ')[1]
        cost = float(cost)
        return cost, sim_std_err


def simulate(simulator, model, init, solver, param=None, init_out=None, 
        output_mode = 2):
    """ Perform a simulation with the Organism simulator
    This will initially create a bunch of temporary files which
    will be used as input to the external call to Organism.
    It will then make the external call, check for errors, 
    and process the std_out to a python object. 

    :simulator: string specifying where the organism simulator binary can be 
                accessed
    :model: path to model file
    :init: np.array of initial concentrations. 
    :solver: list specifying the solver to use 
    :param: 1D list or array of parameter values.
    :init_out: string specifying where to save an input file 
    :returns: sim_std_out, sim_std_err 

    """

    with tempfile.NamedTemporaryFile(mode='w') as solver_tempfile, \
         tempfile.NamedTemporaryFile(mode='w') as init_tempfile, \
         tempfile.NamedTemporaryFile(mode='w') as model_tempfile, \
         tempfile.NamedTemporaryFile(mode='w') as param_tempfile: 
        write_solver(solver_tempfile, solver)
        write_init(init_tempfile, init)
        model_tempfile.write(model)
        model_tempfile.flush()


        command = [simulator, model_tempfile.name, init_tempfile.name, solver_tempfile.name]

        if param != None:
            param_tempfile.write(' '.join(str(i) for i in param))
            param_tempfile.flush()
            command.append('-parameter_input')
            command.append(param_tempfile.name)

        if init_out:
            command.append('-init_output')
            command.append(init_out)

        # Make the external call to Organism
        simulation = sub.Popen(command, stdout=sub.PIPE, stderr=sub.PIPE)
        sim_std_out, sim_std_err = simulation.communicate()

        sim_std_out = sim_std_out.decode() # python 3 compatibility
        sim_std_err = sim_std_err.decode() # python 3 compatibility

        if simulation.returncode != 0: 
            err_msg = "{} \nExit code: {}".format(sim_std_err.strip(), 
                                                simulation.returncode)
            raise Exception(err_msg)

    # Transform the structure of the data object
    sim = sim_result_to_array(sim_std_out, output_mode = output_mode)

    return sim, sim_std_err


def sim_result_to_array(sim_std_out, output_mode=2):
    """ Convert the result from the Simulator (a massive string) to an array.

    TODO: Make this play nice with the outputs from Template solvers.
          Maybe use the solver as an agruments and decipher from that?
    
    :returns: sim[time_index, cell_index, value_index] -- a 3D np.array.
              The value_index 0 and 1 correspond to time and cell number.
              The rest of the value indices correspond to the output given
              by the simulator output mode, as specified in the solver file.
    """
    sim_tmp = []
    
    if output_mode == 2: # Organism's gnuplot mode
        time_index = 0
        cell_id_index = 2
        for line in clean_file_parsing(sim_std_out.split('\n')):
            sim_tmp.append([float(i) for i in line.split()])

    elif output_mode == 1: # Organisms Newman mode
        time_index = 2

        time = None
        new_time = False
        first_line = True
        cell_number = 0
        time_point_index = -1
        for line in sim_std_out.split('\n'):
            if not first_line:
                line = line.rstrip()
                if line:
                    if new_time:
                        time = line.split()[time_index]
                        cell_number = 0
                        time_point_index += 1
                    else:
                        species = [time_point_index, time, cell_number]
                        species.extend(line.split())
                        sim_tmp.append([float(i) for i in species])
                        cell_number += 1
                    new_time = False
                else:
                    new_time = True
            else:
                new_time = True
                first_line = False
    sim_tmp = np.array(sim_tmp, ndmin = 2)

    '''
    Now sim_tmp is a 2D array which would be inconvenient to work with.
    The following bit creates sim, a 3D array where you can access values by
    specifying time_index, cell, and value_index.
    '''
    nr_times = int(sim_tmp[-1, 0]) + 1
    nr_cells = int(sim_tmp[-1, 2]) + 1
    nr_species = sim_tmp.shape[1] - 1
    sim = np.empty((nr_times, nr_cells, nr_species))
    for row in sim_tmp:
        for i, element in enumerate(row[1:]):
            sim[int(row[0]), int(row[2]), i] = element

    #TODO add parser for other output_mode
    return sim


def read_parameter(parameter_file_location):
    """ Read a parameter file and return it as a list.

    :parameter_file_location: string which specifies where to read the file.
    :returns: param -- a list of the parameters. 
    """
    param=[]
    with open(parameter_file_location, 'r') as parameter_file:
        for line in clean_file_parsing(parameter_file):
            param.append([float(entry) for entry in line.split()])
    param = [ entry for sublist in param for entry in sublist] # Flatten
    return param


def param_from_opt(opt_file):
    """Get the best parameter set from a data file resulting from an Organism
    optimisation. The function opens the file, grabs the second-to-last line 
    and splices off the cost-value. 

    :opt_file: string with location of opt_file
    :returns: list of parameters followed by a float with its cost value.

    """
    with open(opt_file, "r") as f:
        all_lines = f.read().splitlines()
    assert (all_lines[-2][0] == "#"), \
            "File does not seem to follow the format for a finished " + \
            "Organism optimisation.\n" 
    param = all_lines[-1].split()
    cost = param.pop(0)
    return param, cost


def optimise(optimiser_path, model, solver, estimator, optimiser, *args):
    """TODO: Docstring for optimise.

    :model: TODO
    :: TODO
    :returns: TODO

    """
    pass


class Costtemplate:
    """ A class containing information and functions for one costtemplate. """
    def __init__(self, nr_cells, nr_vars):
        """ Instantiate one costtemplate.

        :nr_cells: (int) number of cells. 
        :nr_vars: (int) number of variables for each cell. 

        """
        self.__nr_cells = nr_cells
        self.__nr_vars = nr_vars
        self.__template = {}


    def add_target(self, time, variable_index, value_list):
        """Add a cost target for a given variable at a given time.  
        Note: if you call this function twice, with the same time and 
        variable index, the target values will be overwritten. 

        :time: the time for evaluation
        :variable_index: (int) species index according to model file. 
        :value_list: list of targets. Must have len()==nr_cells.

        """
        assert (len(value_list)==self.__nr_cells), \
                "value_list must be as long as there are cells"
        try:
            self.__template[time][variable_index] = value_list
        except KeyError:
            self.__template[time] = {variable_index:value_list}


    def get_costtemplate(self):
        """ get a formatted cost template as a list of lists

        :returns: list of lists, corresponding to lines in a cost-template file.

        """
        template = self.__template
        nr_cells = self.__nr_cells
        nr_vars = self.__nr_vars
        fmt_template = []
        first_iter = True
        for time in sorted(template):
            if first_iter:
                first_keys = template[time].keys()
                first_iter = False
            else:
                if set(first_keys) != set(template[time].keys()):
                    print("WARNING: you have specified cost targets which ", \
                            "are not explicitly set.")
                    print("All targets should be specified for all timepoints.")
            
            fmt_template.append([time, nr_cells, nr_vars])
            block = np.zeros([nr_cells, nr_vars])
            for target_index, target_list in template[time].items():
                block[:,target_index] = np.array(target_list).T
            for line in block:
                fmt_template.append(line)
            fmt_template.append([0])
        self.__formatted_template = fmt_template
        return fmt_template


    def get_costtemplate_lines(self):
        """Use to iterate over the lines of a formatted costtemplate. 
        You must run get_costtemplate() before you can do this.
        
        :yields: lines of the costtemplate.

        """
        fmt_template = self.__formatted_template
        for line in fmt_template:
            line_string = ' '.join((str(element) for element in line))
            yield line_string


    def get_target_indices(self):
        """Get a list of the variable indices which are used as cost targets
        :returns: list of variable indices.

        """
        template = self.__template
        first_iter = True
        var_indices = []
        for time in sorted(template):
            if first_iter:
                var_indices = list(template[time].keys())
                first_iter = False
            else:
                if set(var_indices) != set(template[time].keys()):
                    print("WARNING: you have specified cost targets which ", \
                            "are not explicitly set.")
                    print("All targets should be specified for all timepoints.")
                    print("Otherwise zero will be used as a target.")
        return var_indices


    def print_to_file(self,  file_object):
        """Print the costfunction to a file.
        This will overwrite any existing file.
        It also requires that you have run get_costtemplate() before.
        This must also be done after any changes to the class instance. 

        :file_object: a file object to where the costtemplate will be printed.

        """
        try:
            self.__formatted_template
        except NameError:
            print("Error: you must run get_costtemplate() before you can use" +\
                    " print_to_file()")
            raise

        #with open(file_name, 'w') as f:
        for line in self.get_costtemplate_lines():
            file_object.write(line + '\n')
        file_object.flush()

class CostFunction:
    """ Facilitate the use of external optimisers.
    
    External optmisers will often require a specific format on the function 
    you pass for it to optimise. This class makes it easier to manage.

    The class and its functions was originally written to facilitate use of the 
    python package cma (pip install cma). 
    
    """
    def __init__(self, estimator_binary, model, solver, estimator, param, 
            param_to_opt):
        """Initialise by specifying which configuration for Organism to use. 

        :estimator_binary: TODO
        :model: TODO
        :solver: TODO
        :estimator: TODO
        :param: TODO

        """
        self.est_bin = estimator_binary
        self.model = model
        self.solver = solver
        self.estimator = estimator
        self.original_param = param
        self.param_to_opt = param_to_opt

    def set_solver(self, solver):
        self.solver = solver

    def map_param(self, opt_param):
        """TODO: Docstring for __map_param.

        :opt_param: TODO
        :returns: TODO

        """
        param = copy.deepcopy(self.original_param)

        i = 0
        for key in sorted(self.param_to_opt):
            param[int(key)] = opt_param[i]
            i += 1
        assert (i == opt_param.shape[0]), "ERROR: parameter mapping failed."

        param = [float(i) for i in param]

        return param

    def set_precond(self, precond_function):
        """Add a parameter preconditioner function. 

        precond_funciton :: a function which manipulates an inputed list of 
                            parameters from the optimiser. 
        """
        self.precond_function = precond_function

    def map_param_exponents(self, opt_param_exponents):
        """TODO: Docstring for __map_param.

        :opt_param: TODO
        :returns: TODO

        """
        param = copy.deepcopy(self.original_param)
        base_ten = 10*np.ones_like(opt_param_exponents)
        opt_param = base_ten**opt_param_exponents

        i = 0
        for key in sorted(self.param_to_opt):
            param[int(key)] = opt_param[i]
            i += 1
        assert (i == opt_param.shape[0]), "ERROR: parameter mapping failed."

        param = [float(i) for i in param]

        return param


    def cost_function(self, opt_param):
        """TODO: Docstring for get_cost.

        :opt_param: TODO
        :returns: TODO
        """
        param = copy.deepcopy(opt_param)
        try:
            param = self.precond_function(opt_param)
        except:
            print("no parameter preconditioner, consider adding one")
        param = self.map_param(param)
        cost, log = get_cost(self.est_bin, self.model, self.solver, 
                self.estimator, param=param)
        return cost


class Estimator():
    """Handles the construction of an Organism estimator file."""
    def __init__(self):
        """Initate variables."""
        self.costtemplates = []
        self.init_objects = []

    def add_cost(self, costtemplate, init):
        """TODO: Docstring for add_costtemplate.

        :costtemplate: TODO
        :returns: TODO

        """
        self.costtemplates.append(costtemplate) 
        self.init_objects.append(init)

    def get_costtemplates(self):
        """ allows you to iterate over all the costtemplates.

        :yields: a costtemplate class instance
        """
        for cost in self.costtemplates:
            yield cost

    def nr_files_needed(self):
        """ Get the number of tempfiles that you need for cost and init files.
        """
        assert len(self.costtemplates) == len(self.init_objects)
        return len(self.costtemplates) + len(self.init_objects)

    def write_to_file(self, file_object, files):
        """Write the estimator instance to a supplied file object.

        :file_object: open file object to write the estimator file to.
        :files: list of file objects to write the costtemplates and init files to. 
        """
        costtemplate_file_objects = files[:int(len(files)/2)]
        init_file_objects = files[int(len(files)/2):]

        assert len(files) == len(self.costtemplates) + len(self.init_objects), \
        "You must provide one file-handle for each costtemplate and init-" + \
        "\nobject in the Estimator instance"

        file_object.write("{}\n".format(len(self.costtemplates)))

        species_in_template = []
        for i, costtemplate in enumerate(self.costtemplates):
            species_in_template.append( costtemplate.get_target_indices() )
            costtemplate.get_costtemplate()
            costtemplate.print_to_file(costtemplate_file_objects[i])

        for i in species_in_template:
            file_object.write("{}\n".format(len(i)))

        for i in costtemplate_file_objects:
            file_object.write("{}\n".format(i.name))

        for i in species_in_template:
            file_object.write("{}\n".format(' '.join([str(j) for j in i])))
        
        file_object.write("{}\n".format(len(self.init_objects)))

        for i, init in enumerate(self.init_objects): 
            write_init(init_file_objects[i], init)
            file_object.write("{}\n".format(init_file_objects[i].name))

        file_object.flush()







